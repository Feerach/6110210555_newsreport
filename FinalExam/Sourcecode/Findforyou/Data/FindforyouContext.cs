using Findforyou.Models; 
using Microsoft.EntityFrameworkCore; 
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
 
namespace Findforyou.Data 
{     public class FindforyouContext : IdentityDbContext<MyDataUser>     
      {         
          public DbSet<MyData> MyDataList { get; set; }         
          public DbSet<Location> Location{ get; set; }                  
          
          protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)         
          {             
              optionsBuilder.UseSqlite(@"Data source= Findforyou.db");                  
          }     
       } 
} 