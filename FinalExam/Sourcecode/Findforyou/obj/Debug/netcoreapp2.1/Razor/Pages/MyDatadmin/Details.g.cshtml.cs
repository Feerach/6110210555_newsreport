#pragma checksum "D:\Findforyou\Pages\MyDatadmin\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "775397bad4cc23a4ca3d9f8415fca455ef37dc72"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(Findforyou.Pages.MyDatadmin.Pages_MyDatadmin_Details), @"mvc.1.0.razor-page", @"/Pages/MyDatadmin/Details.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/MyDatadmin/Details.cshtml", typeof(Findforyou.Pages.MyDatadmin.Pages_MyDatadmin_Details), null)]
namespace Findforyou.Pages.MyDatadmin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Findforyou\Pages\_ViewImports.cshtml"
using Findforyou;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"775397bad4cc23a4ca3d9f8415fca455ef37dc72", @"/Pages/MyDatadmin/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"238fe40194422dee668a7e1f745ff8e26c8cc5da", @"/Pages/_ViewImports.cshtml")]
    public class Pages_MyDatadmin_Details : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "./Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "./Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(56, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 4 "D:\Findforyou\Pages\MyDatadmin\Details.cshtml"
  
    ViewData["Title"] = "รายละเอียดผู้ใช้";

#line default
#line hidden
            BeginContext(110, 189, true);
            WriteLiteral("\r\n<h2>รายละเอียดผู้ใช้</h2>\r\n\r\n<div>\r\n    <h4>รายละเอียด</h4>\r\n    <hr />\r\n    <dl class=\"dl-horizontal\">\r\n        <dt>\r\n            ชื่อร้านอาหาร\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(300, 61, false);
#line 18 "D:\Findforyou\Pages\MyDatadmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MyData.MyDataCat.LocationName));

#line default
#line hidden
            EndContext();
            BeginContext(361, 95, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n           วันนัดหมาย\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(457, 43, false);
#line 24 "D:\Findforyou\Pages\MyDatadmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MyData.Date));

#line default
#line hidden
            EndContext();
            BeginContext(500, 95, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ชื่อ-สกุล\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(596, 43, false);
#line 30 "D:\Findforyou\Pages\MyDatadmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MyData.Name));

#line default
#line hidden
            EndContext();
            BeginContext(639, 94, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ชื่อเล่น\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(734, 47, false);
#line 36 "D:\Findforyou\Pages\MyDatadmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MyData.NickName));

#line default
#line hidden
            EndContext();
            BeginContext(781, 96, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n           เวลานัดหมาย\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(878, 43, false);
#line 42 "D:\Findforyou\Pages\MyDatadmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MyData.Time));

#line default
#line hidden
            EndContext();
            BeginContext(921, 105, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            รายละเอียดเพิ่มเติม\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1027, 45, false);
#line 48 "D:\Findforyou\Pages\MyDatadmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MyData.Detail));

#line default
#line hidden
            EndContext();
            BeginContext(1072, 94, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            Facebook\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1167, 47, false);
#line 54 "D:\Findforyou\Pages\MyDatadmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MyData.Facebook));

#line default
#line hidden
            EndContext();
            BeginContext(1214, 99, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            เบอร์โทรศัพท์\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1314, 49, false);
#line 60 "D:\Findforyou\Pages\MyDatadmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MyData.TellNumber));

#line default
#line hidden
            EndContext();
            BeginContext(1363, 47, true);
            WriteLiteral("\r\n        </dd>\r\n    </dl>\r\n</div>\r\n<div>\r\n    ");
            EndContext();
            BeginContext(1410, 67, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e6916e1463424d94b465164418076a09", async() => {
                BeginContext(1469, 4, true);
                WriteLiteral("Edit");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 65 "D:\Findforyou\Pages\MyDatadmin\Details.cshtml"
                           WriteLiteral(Model.MyData.MyDataID);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1477, 8, true);
            WriteLiteral(" |\r\n    ");
            EndContext();
            BeginContext(1485, 38, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d3001d2ddfd2484182a9f42b330f1e86", async() => {
                BeginContext(1507, 12, true);
                WriteLiteral("Back to List");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1523, 10, true);
            WriteLiteral("\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Findforyou.Pages.MyDatadmin.DetailsModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Findforyou.Pages.MyDatadmin.DetailsModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Findforyou.Pages.MyDatadmin.DetailsModel>)PageContext?.ViewData;
        public Findforyou.Pages.MyDatadmin.DetailsModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
