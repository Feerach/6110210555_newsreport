using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Findforyou.Data;
using Findforyou.Models;

namespace Findforyou.Pages.MyDatadmin
{
    public class EditModel : PageModel
    {
        private readonly Findforyou.Data.FindforyouContext _context;

        public EditModel(Findforyou.Data.FindforyouContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MyData MyData { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MyData = await _context.MyDataList
                .Include(m => m.MyDataCat).FirstOrDefaultAsync(m => m.MyDataID == id);

            if (MyData == null)
            {
                return NotFound();
            }
           ViewData["LocationID"] = new SelectList(_context.Location, "LocationID", "LocationName");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(MyData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MyDataExists(MyData.MyDataID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MyDataExists(int id)
        {
            return _context.MyDataList.Any(e => e.MyDataID == id);
        }
    }
}
