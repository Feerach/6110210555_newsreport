using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Findforyou.Data;
using Findforyou.Models;

namespace Findforyou.Pages.MyDatadmin
{
    public class CreateModel : PageModel
    {
        private readonly Findforyou.Data.FindforyouContext _context;

        public CreateModel(Findforyou.Data.FindforyouContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["LocationID"] = new SelectList(_context.Location, "LocationID", "LocationName");
            return Page();
        }

        [BindProperty]
        public MyData MyData { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.MyDataList.Add(MyData);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}