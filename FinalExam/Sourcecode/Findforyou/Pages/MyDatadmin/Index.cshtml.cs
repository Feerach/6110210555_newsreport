using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Findforyou.Data;
using Findforyou.Models;

namespace Findforyou.Pages.MyDatadmin
{
    public class IndexModel : PageModel
    {
        private readonly Findforyou.Data.FindforyouContext _context;

        public IndexModel(Findforyou.Data.FindforyouContext context)
        {
            _context = context;
        }

        public IList<MyData> MyData { get;set; }

        public async Task OnGetAsync()
        {
            MyData = await _context.MyDataList
                .Include(m => m.MyDataCat).ToListAsync();
        }
    }
}
