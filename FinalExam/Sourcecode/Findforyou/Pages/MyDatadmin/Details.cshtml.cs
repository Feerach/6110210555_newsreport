using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Findforyou.Data;
using Findforyou.Models;

namespace Findforyou.Pages.MyDatadmin
{
    public class DetailsModel : PageModel
    {
        private readonly Findforyou.Data.FindforyouContext _context;

        public DetailsModel(Findforyou.Data.FindforyouContext context)
        {
            _context = context;
        }

        public MyData MyData { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MyData = await _context.MyDataList
                .Include(m => m.MyDataCat).FirstOrDefaultAsync(m => m.MyDataID == id);

            if (MyData == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
