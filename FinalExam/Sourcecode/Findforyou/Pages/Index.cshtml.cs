﻿using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Threading.Tasks; 
using Microsoft.AspNetCore.Mvc; 
using Microsoft.AspNetCore.Mvc.RazorPages; 
 
using Microsoft.EntityFrameworkCore; 
using Findforyou.Data; 
using Findforyou.Models; 
 
namespace Findforyou.Pages 
{     
    public class IndexModel : PageModel     
    {         
        private readonly Findforyou.Data.FindforyouContext _context; 
 
        public IndexModel(Findforyou.Data.FindforyouContext context)        
        {             
            _context = context;         
        }                  
        public IList<MyData> MyData { get;set; } 
        public IList<Location> Location { get;set; }
 
        public async Task OnGetAsync()         
        {             
            MyData = await _context.MyDataList                 
                .Include(n => n.MyDataCat).ToListAsync();  

            Location = await _context.Location.ToListAsync(); 
        
        }         
    } 
}