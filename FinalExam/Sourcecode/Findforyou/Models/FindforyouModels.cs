using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Findforyou.Models
{
        public class MyDataUser :  IdentityUser{         
            public string  FirstName { get; set;}         
            public string  LastName { get; set;}     
        }   
        public class Location{
            public int LocationID{get;set;}
            public string LocationName{get;set;}
            public string LocationDetail{get;set;}
        }

        public class MyData{
            public int MyDataID {get;set;}

            public int LocationID{get;set;}
            public Location MyDataCat{get;set;}

            [DataType(DataType.Date)]
            public string Date{get;set;}
            public string Name{get;set;}
            public string NickName{get;set;}
            public string Time{get;set;}
            public string Detail{get;set;}
            public string Facebook{get;set;}
            public string TellNumber{get;set;}

            public string MyDataUserId {get; set;}         
            public MyDataUser  postUser {get; set;}
        }
}